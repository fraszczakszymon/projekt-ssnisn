function filterScale = getFilterScale()

    filterScale = str2double(get(findobj('Tag','filterText'), 'String'));
    filterScale = filterScale(1);
    
end