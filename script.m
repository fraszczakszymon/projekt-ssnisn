clear

orginalPicture = imread('pic.png');

picture = rgb2gray(orginalPicture);
[width height] = size(picture);


binPictureVector = de2bi(picture)';
binPictureVector2 = binPictureVector(:);

binPictureVector3 = vec2mat(binPictureVector2,height)';

noisedPicture = imnoise(orginalPicture, 'gaussian',0.5);

binNoisedPictureVector = de2bi(noisedPicture)';
binNoisedPictureVector = binNoisedPictureVector(:);
binNoisedPictureVector = vec2mat(binNoisedPictureVector,height)';

result = [];

for i=1:height*8
    
    vector = binPictureVector3(:,i);
    vector2 = binNoisedPictureVector(:,i);
    
    vert = double(vector);
    vert2 = {double(vector2)};
    
    network = newhop(vert);
    
    [r, p, a] = sim(network,{1,2},{},vert2);

    result = [result; r{1}];

end

result = uint8(result)

restoredPictureVector = bi2de(vec2mat(result,8));

decPicture = vec2mat(restoredPictureVector, width)';

imshow(picture);
figure
imshow(noisedPicture);
figure
imshow(decPicture);
