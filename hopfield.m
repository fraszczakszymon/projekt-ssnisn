function varargout = hopfield(varargin)

    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @hopfield_OpeningFcn, ...
                       'gui_OutputFcn',  @hopfield_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end


function varargout = hopfield_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;

    
function filterSlider_CreateFcn(hObject, eventdata, handles)
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

function scaleSlider_CreateFcn(hObject, eventdata, handles)
    if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor',[.9 .9 .9]);
    end

function scaleText_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
function iterationSlider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

    
function hopfield_OpeningFcn(hObject, eventdata, handles, varargin)
    handles.output = hObject;
    guidata(hObject, handles);

    for i = 0 : 9
       imshow(getImage(i, 1), 'Parent',findobj('Tag', strcat('image', int2str(i))))
       set(findobj('Tag', strcat('radiobutton', int2str(i))), 'Callback', {@refreshPatern, handles})
       set(findobj('Tag', strcat('checkbox', int2str(i))), 'Callback', {@nothing})
    end
    refreshPatern(hObject, eventdata, handles)

function scaleSlider_Callback(hObject, eventdata, handles)
    set(handles.scaleText, 'String', round(get(handles.scaleSlider, 'Value')));
    refreshPatern(hObject, eventdata, handles)

    
function filterSlider_Callback(hObject, eventdata, handles)
    set(handles.filterText, 'String', get(handles.filterSlider, 'Value'));
    refreshPatern(hObject, eventdata, handles)

function filterType_Callback(hObject, eventdata, handles)
    refreshPatern(hObject, eventdata, handles)

function iterationSlider_Callback(hObject, eventdata, handles)
    set(handles.iterationText, 'String', round(get(handles.iterationSlider, 'Value')));

    
function trainNetwork_Callback(hObject, eventdata, handles)
    tic
    network = newhop(getVectorForTeaching());
    handles.network = network;
    guidata(hObject, handles);

    [r, p, a] = sim(network,{1,getIterationNumber()},{},{convertImageToVector(handles.patern)});
    toc
    
    axes(handles.hopfieldOutput);
    for i = 1 : getIterationNumber(),
        decPicture = convertVectorToImage(r{i});
        [x y z] = size(decPicture);

        border = zeros(x+2,y+2);
        newImage = border;
        newImage(2:x+1, 2:y+1) = decPicture;
        newImage = uint8(newImage);

        new = cat(3, newImage, newImage, newImage);

        newImage = border + 255;
        newImage(2:x+1, 2:y+1) = decPicture;
        newImage = uint8(newImage);

        if isequal(decPicture, handles.image)
            new(:,:,2) = newImage;
        else
            new(:,:,1) = newImage;
        end


        subplot(5, 5, i);
        subimage(new);  

    end
