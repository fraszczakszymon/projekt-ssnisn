function selectedFilter = getSelectedFilter()
    
    filter = findobj('Tag', 'filterType');
    filterList = get(filter, 'String');
    filterValue = get(filter, 'Value');
    selectedFilter = filterList{filterValue};

end
