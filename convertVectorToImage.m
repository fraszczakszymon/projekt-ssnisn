function image = convertVectorToImage(vector)

    image = bi2de(vec2mat(uint8(vector),8));
    image = vec2mat(image, getImageSize())';

end

