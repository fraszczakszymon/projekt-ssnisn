function imageSize = getImageSize()

    imageSize = str2num(get(findobj('Tag','scaleText'), 'String'));
    imageSize = imageSize(1);
    
end

