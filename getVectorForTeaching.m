function vectorForTeaching = getVectorForTeaching()
    vectorForTeaching = [];
    for i = 0 : 9
        if get(findobj('Tag', strcat('checkbox', int2str(i))), 'Value') == 1
            vectorForTeaching = [vectorForTeaching convertImageToVector(getImage(i, getImageScale))];
        end
    end
end
