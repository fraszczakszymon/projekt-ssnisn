function refreshPatern(hObject, eventdata, handles)

    selectedPatern = getSelectedPatern();
    imageScale = getImageScale();
    image = getImage(selectedPatern, imageScale);
    handles.image = image;
    guidata(hObject, handles);
    imshow(image, 'Parent', handles.imagePaternOrginal);
    
    filter = getSelectedFilter();
    filterScale = getFilterScale();
    
    patern = imnoise(image, filter, filterScale);
    handles.patern = patern;
    guidata(hObject, handles);
    imshow(patern, 'Parent', handles.imagePatern);
    
end

