function imageScale = getImageScale()

    imageScale = str2num(get(findobj('Tag','scaleText'), 'String'))/34;
    imageScale = imageScale(1);
    
end

