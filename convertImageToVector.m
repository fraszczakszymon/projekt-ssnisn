function vector = convertImageToVector(image)

    vector = de2bi(image)';
    vector = vector(:);
    vector = double(vector);

end

