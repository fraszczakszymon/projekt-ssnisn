function image = getImage(imageNumber, scale)

    image = rgb2gray(imresize(imread(strcat(int2str(imageNumber), '.png')), scale));
    
end

