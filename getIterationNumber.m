function iteration = getIterationNumber()

    iteration = str2num(get(findobj('Tag','iterationText'), 'String'));
    iteration = iteration(1);
    
end

